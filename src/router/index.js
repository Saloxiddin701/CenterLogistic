import { createRouter, createWebHashHistory } from 'vue-router'
import Home from '../Sections/HomePage/homePage'

const routes = [
    {
        path: '/',
        name: 'home',
        component: Home
    },
    {
        path: '/newsPage',
        name: 'NewsPage',
        component: () => import('../Sections/NewsPage')
    },
    {
        path: '/privacy',
        component: () => import('../Sections/Privacy')
    }
]

const router = createRouter({
    history: createWebHashHistory(),
    routes
})

export default router